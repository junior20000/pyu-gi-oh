from base64 import b64encode

from flask import Flask, request, jsonify, Response, json, render_template

from model.database import db, ma
import model.entity.ultrabank as ultrabank

import os

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = "sqlite:///teste2.db"
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.config["JSON_SORT_KEYS"] = False
app.config['TEMPLATES_AUTO_RELOAD'] = True
app.jinja_env.auto_reload = True

db.init_app(app)
ma.init_app(app)

@app.route('/yugioh')
def home():
    return "YUGIOH!"

@app.route('/yugioh/<card_id>')
def card_by_id(card_id: str):
    result: ultrabank.Card = db.session.query(ultrabank.Card).filter_by(card_id=card_id).first()
    monster_attr_pic = None

    if not result:
        return "card not found"

    if isinstance(result, ultrabank.CardMonster):
        monster_attr_pic = b64encode(result.MonsterAttr.picture).decode("utf8")

    return render_template("card.html", card=result, card_pic=b64encode(result.picture).decode('utf8'), monster_attr_pic=monster_attr_pic)

@app.route('/api/')
def hello_world():
    return 'ORICHALCOS'


@app.route('/api/cards', methods=["GET"])
def getAllCards2():
    data = []

    for row in ultrabank.Card.query.all():
        if isinstance(row, ultrabank.CardSpell):
            data.append(ultrabank.CardSpellSchema().dump(row))
        elif isinstance(row, ultrabank.CardTrap):
            data.append(ultrabank.CardTrapSchema().dump(row))
        elif isinstance(row, ultrabank.CardMonster):
            data.append(ultrabank.CardMonsterSchema().dump(row))
    r = Response(response=json.dumps(data), status=200, mimetype="application/json")
    r.headers["Content-Type"] = 'application/json; charset=utf-8'
    return r


@app.route('/api/card/<id>')
def get_card_by_id(id: int):
    result = ultrabank.Card.query.get(id)

    if isinstance(result, ultrabank.CardSpell):
        return ultrabank.CardSpellSchema().jsonify(result)
    elif isinstance(result, ultrabank.CardTrap):
        return ultrabank.CardTrapSchema().jsonify(result)
    elif isinstance(result, ultrabank.CardMonster):
        return ultrabank.CardMonsterSchema().jsonify(result)

    return "card not found"

if __name__ == '__main__':
    app.run()
