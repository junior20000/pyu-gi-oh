#  Copyright 2020, Roberto Schiavelli Júnior - All Rights Reserved
#  Unauthorized copying of this file (content included) or any other within this project, via any medium is strictly prohibited
#  Proprietary and confidential
#  Written by Roberto Schiavelli Júnior
#  Last time modified: 20/06/2020 09:31.

from marshmallow import fields, pre_dump, post_dump, post_load

from ..database import db, ma
from ..custom_fields import Blob


def partition(items, key, container_name):
    return [
        {
            key: index,
            container_name: [item for item in items if item[key] == index],
        }
        for index in set(item[key] for item in items)
    ]

class CardStatus(db.Model):
    __tablename__ = "CardStatus"

    id = db.Column('id', db.Integer, primary_key=True, autoincrement=True)
    name = db.Column('name', db.TEXT, nullable=False)
    desc = db.Column('description', db.TEXT)

    card_rule_set = db.relationship("CardRuleCard", backref='CardStatus', lazy="select")


class CardRule(db.Model):
    __tablename__ = "CardRule"

    id = db.Column('id', db.Integer, primary_key=True, autoincrement=True)
    name = db.Column('name', db.TEXT, nullable=False)
    desc = db.Column('description', db.TEXT)

    card_rule_set = db.relationship("CardRuleCard", backref='CardRule', lazy="select")


class RuleFormat(db.Model):
    __tablename__ = "RuleFormat"

    id = db.Column('id', db.Integer, primary_key=True, autoincrement=True)
    name = db.Column('name', db.TEXT, nullable=False)
    desc = db.Column('description', db.TEXT)

    card_rule_set = db.relationship("CardRuleCard", backref='RuleFormat', lazy="select")

class CardRuleCard(db.Model):
    __tablename__ = "CardRule_Card"

    card_rule_id = db.Column("CardRule_id", db.ForeignKey("CardRule.id"), primary_key=True)
    card_id = db.Column("Card_id", db.ForeignKey("Card.id"), primary_key=True)
    ruleFormat = db.Column("ruleFormat", db.ForeignKey("RuleFormat.id"), primary_key=True)
    status = db.Column("status", db.ForeignKey("CardStatus.id"), nullable=False)

    """cards = db.relationship("Card", backref="CardRuleCard", lazy="select")
    rules = db.relationship("CardRule", backref="CardRuleCard", lazy="select")
    rules_format = db.relationship("RuleFormat", backref="CardRuleCard", lazy="select")"""

    #__table_args__ = (db.UniqueConstraint(card_rule_id, card_id, ruleFormat),)


class Card(db.Model):
    __tablename__ = "Card"

    id = db.Column('id', db.Integer, primary_key=True, autoincrement=True)
    card_id = db.Column('cardid', db.TEXT, nullable=False)
    name = db.Column('name', db.TEXT, nullable=False)
    desc = db.Column('description', db.TEXT)
    edition = db.Column('edition', db.TEXT)
    type = db.Column('card_type', db.Integer, db.ForeignKey('CardType.id'), nullable=False)
    source = db.Column('card_source', db.Integer, db.ForeignKey('CardSource.id'), nullable=False)
    picture = db.Column('picture', db.BLOB)

    situation = db.relationship("CardRuleCard", backref="Card", lazy="select")

    __mapper_args__ = {'polymorphic_on': type}


class CardType(db.Model):
    __tablename__ = "CardType"

    id = db.Column('id', db.Integer, primary_key=True, autoincrement=True)
    name = db.Column('name', db.TEXT, nullable=False)
    desc = db.Column('description', db.TEXT)
    picture = db.Column('picture', db.BLOB)

    cards = db.relationship("Card", backref='CardType', lazy="select")


class CardSource(db.Model):
    __tablename__ = "CardSource"

    id = db.Column('id', db.Integer, primary_key=True, autoincrement=True)
    name = db.Column('name', db.TEXT, unique=True, nullable=False)
    desc = db.Column('description', db.TEXT)

    cards = db.relationship("Card", backref="CardSource", lazy="select")


class CardSpell(Card):
    __tablename__ = "CardSpell"
    __mapper_args__ = {"polymorphic_identity": 2}

    id = db.Column('id_type', db.ForeignKey("Card.id"), primary_key=True)
    spell_type = db.Column("spell_type", db.ForeignKey("SpellType.id"), nullable=False)


class SpellType(db.Model):
    __tablename__ = "SpellType"

    id = db.Column('id', db.Integer, primary_key=True, autoincrement=True)
    name = db.Column('name', db.TEXT, nullable=False)
    desc = db.Column('description', db.TEXT)
    picture = db.Column('picture', db.BLOB)

    spell_cards = db.relationship("CardSpell", backref="SpellType")


class CardTrap(Card):
    __tablename__ = "CardTrap"
    __mapper_args__ = {"polymorphic_identity": 3}

    id = db.Column('id_type', db.ForeignKey("Card.id"), primary_key=True)
    trap_type = db.Column("trap_type", db.ForeignKey("TrapType.id"), nullable=False)


class TrapType(db.Model):
    __tablename__ = "TrapType"

    id = db.Column('id', db.Integer, primary_key=True, autoincrement=True)
    name = db.Column('name', db.TEXT, nullable=False)
    desc = db.Column('description', db.TEXT)
    picture = db.Column('picture', db.BLOB)

    trap_cards = db.relationship("CardTrap", backref="TrapType")


class MonsterTypeCardMonster(db.Model):
    __tablename__ = "MonsterType_CardMonster"

    monster_type_id = db.Column("MonsterType_id", db.ForeignKey("MonsterType.id"), primary_key=True)
    card_id = db.Column("CardMonster_id", db.ForeignKey("CardMonster.id_type"), primary_key=True)


class CardMonster(Card):
    __tablename__ = "CardMonster"
    __mapper_args__ = {"polymorphic_identity": 1}

    id = db.Column('id_type', db.ForeignKey("Card.id"), primary_key=True)
    level_star = db.Column('level_star', db.Integer, nullable=False)
    atk = db.Column('atk', db.Integer, nullable=False)
    defense = db.Column('def', db.Integer, nullable=False)
    monster_attr = db.Column('monster_attr', db.ForeignKey("MonsterAttr.id"), nullable=False)

    monster_types = db.relationship("MonsterType", secondary="MonsterType_CardMonster",
                                    backref=db.backref('monster_cards', lazy='dynamic'))


class MonsterAttr(db.Model):
    __tablename__ = "MonsterAttr"

    id = db.Column('id', db.Integer, primary_key=True, autoincrement=True)
    name = db.Column('name', db.TEXT, nullable=False)
    desc = db.Column('description', db.TEXT)
    picture = db.Column('picture', db.BLOB)

    monster_cards = db.relationship("CardMonster", backref="MonsterAttr")


class MonsterType(db.Model):
    __tablename__ = "MonsterType"

    id = db.Column('id', db.Integer, primary_key=True, autoincrement=True)
    name = db.Column('name', db.TEXT, nullable=False)
    desc = db.Column('description', db.TEXT)
    picture = db.Column('picture', db.BLOB)


# --------------------------------------------------------------------------------------
# ------------------------------------ [ SCHEMAS ] -------------------------------------
# --------------------------------------------------------------------------------------
class RuleFormatSchema(ma.SQLAlchemySchema):
    class Meta:
        model = RuleFormat
        ordered = True

    id = ma.auto_field()
    name = ma.auto_field()
    desc = ma.auto_field()


class CardRuleSchema(ma.SQLAlchemySchema):
    class Meta:
        model = CardRule
        ordered = True

    id = ma.auto_field()
    name = ma.auto_field()
    desc = ma.auto_field()


class CardStatusSchema(ma.SQLAlchemySchema):
    class Meta:
        model = CardStatus
        ordered = True

    id = ma.auto_field()
    name = ma.auto_field()
    desc = ma.auto_field()


class CardTypeSchema(ma.SQLAlchemySchema):
    class Meta:
        model = CardType
        ordered = True

    id = ma.auto_field()
    name = ma.auto_field()
    desc = ma.auto_field()
    picture = Blob()


class CardSourceSchema(ma.SQLAlchemySchema):
    class Meta:
        model = CardSource
        ordered = True

    id = ma.auto_field()
    name = ma.auto_field()
    desc = ma.auto_field()


class RuleSetSchema(ma.SQLAlchemySchema):
    class Meta:
        model = CardRuleCard
        ordered = True

    card_rule_id = fields.Nested(CardRuleSchema, attribute="CardRule", data_key="rule")
    rule_format = fields.Nested(RuleFormatSchema, attribute="RuleFormat")
    status = fields.Nested(CardStatusSchema, attribute="CardStatus")

class CardSchema(ma.SQLAlchemySchema):
    class Meta:
        model = Card
        ordered = True

    id = ma.auto_field()
    card_id = ma.auto_field()
    name = ma.auto_field()
    desc = ma.auto_field()
    edition = ma.auto_field()
    type = fields.Nested(CardTypeSchema, attribute="CardType")
    source = fields.Nested(CardSourceSchema, attribute="CardSource")

    situation = fields.Nested(RuleSetSchema, attribute="situation", many=True)

    picture = Blob()

class MonsterAttrSchema(ma.SQLAlchemySchema):
    class Meta:
        model = MonsterAttr
        ordered = True

    id = ma.auto_field()
    name = ma.auto_field()
    desc = ma.auto_field()
    picture = Blob()


class MonsterTypeSchema(ma.SQLAlchemySchema):
    class Meta:
        model = MonsterType
        ordered = True

    id = ma.auto_field()
    name = ma.auto_field()
    desc = ma.auto_field()
    picture = Blob()


class CardMonsterSchema(CardSchema):
    class Meta:
        model = CardMonster
        ordered = True

    level_star = ma.auto_field()
    atk = ma.auto_field()
    defense = ma.auto_field(data_key="def")
    monster_attr = fields.Nested(MonsterAttrSchema, attribute="MonsterAttr")

    monster_types = fields.List(fields.Nested(MonsterTypeSchema, attribute="MonsterType"), many=True)


class SpellTypeSchema(ma.SQLAlchemySchema):
    class Meta:
        model = SpellType
        ordered = True

    id = ma.auto_field()
    name = ma.auto_field()
    desc = ma.auto_field()
    picture = Blob()


class CardSpellSchema(CardSchema):
    class Meta:
        model = CardSpell
        ordered = True

    spell_type = fields.Nested(SpellTypeSchema, attribute="SpellType")


class TrapTypeSchema(ma.SQLAlchemySchema):
    class Meta:
        model = TrapType
        ordered = True

    id = ma.auto_field()
    name = ma.auto_field()
    desc = ma.auto_field()
    picture = Blob()


class CardTrapSchema(CardSchema):
    class Meta:
        model = CardTrap
        ordered = True

    trap_type = fields.Nested(TrapTypeSchema, attribute="TrapType")
