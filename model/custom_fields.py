#  Copyright 2020, Roberto Schiavelli Júnior - All Rights Reserved
#  Unauthorized copying of this file (content included) or any other within this project, via any medium is strictly prohibited
#  Proprietary and confidential
#  Written by Roberto Schiavelli Júnior
#  Last time modified: 17/06/2020 16:23.

import base64
import typing

from marshmallow import fields, ValidationError


class Blob(fields.Field):

    def _validate(self, value):
        if not isinstance(value, bytes):
            raise ValidationError("Invalid input type. Expected: bytes, provided: %s" % type(value))

    def _serialize(self, value: bytes, attr: str, obj: typing.Any, **kwargs):
        """

        Parameters
        ----------
        value
        attr
        obj
        kwargs

        Returns
        -------

        """
        # TODO: BETTER DOCSTRING

        if not value:
            return ""

        return base64.b64encode(value).decode("utf8")
